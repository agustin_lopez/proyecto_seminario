Version "Hito 2: "

    - menu_mat_fis() = Muestra en pantalla las opciones para acceder a los menu de funciones o física
    
    - menu_lineal_afin() = Muestra por pantalla el menu de funciones lineales y afines. 
    - pendiente() = Retorna la pendiente de una recta. 
    - interseccion_y() = Retorna la interseccion con el eje Y.
    - Interseccion_x() = Retorna la interseccion con el eje X.
    - formula_general_lineal() = Muestra por pantalla la formula general de la recta. 
    - IntersecciondeRectas() = Retorna la interseccion de dos rectas.
    - AreaBajoCurva() = retorna el area bajo la funcion.

    - menu_cuadrica() =  Muestra por pantalla el menu de las funciones cuadraticas.
    - InterseccionXcuadratica() = Muestra las intersecciones con el eje x. 
    - vertice() = Retorna el vertice de la parábola. 
    - maximo_minimo() = Muestra por pantalla si la funcion tiene un minimo o un maximo.
    - concavidad() = Retorna la concavidad de la parabola (+ o -). 
    - discriminante() = Retorna el valor del discriminante.
    
    - menu_fisica() = Muestra en pantalla el menú de Física.
    - info() = Muestra el menú de las funciones de proyectiles.
    - primer_orden = Retorna vector posición en coordenadas i y j.
    - segundo_orden = Es la derivada de primer_orden(). Retorna la velocidad con respecto al tiempo en coordenadas i y j.
    - Ceros() = Retorna el desplazamiento total y el tiempo del proyectil cuando llega a destino.
    - Max_H = Retorna la altura máxima del proyectil.
   

