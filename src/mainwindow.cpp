#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <cmath>
#define g 9.81


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

}

MainWindow::~MainWindow()
{
    delete ui;
}


float a1;
float a2;
float a3;
float a4;
float a5;
float a6;


float areapuntero = NULL;

float area1;
float area;



void MainWindow::on_pushButton_clicked()
{
     QString disc1 = ui->da->text();

        float da = disc1.toFloat();

        QString disc2 = ui->db->text();

        float db = disc2.toFloat();

        QString disc3 = ui->dc->text();

        float dc = disc3.toFloat();
        QString a4 = ui->Xf->text();

        float Xfinal = a4.toFloat();

        QString a5 = ui->Xi->text();

        float Xinicial = a5.toFloat();

        float resu = pow(db,2)- 4*da*dc;

        float x1,x2;

        if(resu > 0.0){

            x1 = (-db + pow((pow(db,2))-4*da*dc,0.5))/2*da;

            x2 = (-db - pow((pow(db,2))-4*da*dc,0.5))/2*da;


            ui->lcdNumber_2->display(x1);
            ui->lcdNumber_4->display(x2);

        }

        else if(resu == 0.0){

            x1 = (-db/2*da);

            ui->lcdNumber_2->display(x1);
            ui->lcdNumber_4->display("NA");

        }

        else{


            ui->lcdNumber_2->display("NA");
            ui->lcdNumber_4->display("NA");

        }

        float vx = -db /(2*da);

        float vy = da*(pow(vx,2))+db*vx+dc;

        float area = (da*(pow(Xfinal,3)/3)-(da*(pow(Xinicial,3)/3)) + (db*(pow(Xfinal,2)/2))- db*(pow(Xinicial,2)/2)) + (dc*Xfinal - dc*Xinicial);

        ui->lcdNumber->display(vx);
        ui->lcdNumber_3->display(vy);
        ui->lcdnumber->display(area);


        QSplineSeries *series = new QSplineSeries();

            for(int w = -10;w<=10;w++){

                series->append(w, (da*pow(w,2) + db*w + dc ));

            }
            QChart *chart = new QChart();
            chart->legend()->hide();
            chart->addSeries(series);
            chart->setTitle("Cuadrática");
            chart->createDefaultAxes();
            chart->axes(Qt::Vertical).first()->setRange(-100, 100);

            QChartView *chartView = new QChartView(chart);
            chartView->setRenderHint(QPainter::Antialiasing);

            ui->graphicsView->setChart(chart);



       //-------------------------------------//

            QSplineSeries *series1 = new QSplineSeries();
                QSplineSeries *series2 = new QSplineSeries();





                for(int c = Xinicial ; c <= Xfinal;c++){

                    series1->append(c,(da*pow(c,2)+db*c + dc));
                    series2->append(c,0);

                }

                QAreaSeries *series0 = new QAreaSeries(series1, series2);
                series0->setName("Área");
                QPen pen(0x059605);
                pen.setWidth(3);
                series0->setPen(pen);

                QLinearGradient gradient(QPointF(0, 0), QPointF(0, 1));
                gradient.setColorAt(0.0, 0x3cc63c);
                gradient.setColorAt(1.0, 0x26f626);
                gradient.setCoordinateMode(QGradient::ObjectBoundingMode);
                series0->setBrush(gradient);

                QChart *chart2 = new QChart();
                chart2->addSeries(series0);
                chart2->setTitle("Área bajo la curva");
                chart2->createDefaultAxes();



                QChartView *chartView2 = new QChartView(chart2);
                chartView2->setRenderHint(QPainter::Antialiasing);

                ui->graphicsView_2->setChart(chart2);


        if (resu == 0){
            ui->disc->setText("Tiene una raíz");
        }

        else if (resu >0){
            ui->disc->setText("Tiene dos raíces.");
        }
        else
        {
            ui->disc->setText("No tiene raíces.");
        }
        }

//---------------------------------------------------------//

void MainWindow::on_pushButton_2_clicked(){
    QString disca = ui->vi->text();

    float vi = disca.toFloat();

    QString discb = ui->angulo->text();

    float angulo = discb.toFloat();

    float radianes = (angulo*M_PI)/180;

    float altura_max = trunc((vi*sin(radianes)*(abs(vi*sin(radianes)/g))-0.5*g*pow((abs(vi*sin(radianes)/g)),2))*100)/100.0f;

    float tiempo_total = trunc((2*vi*sin(radianes)/g)*100)/100.0f;

    float desplazamiento = trunc((vi*cos(radianes)*(2*vi*sin(radianes)/g)*100))/100.0f;;

    float tiempo_max = trunc(abs(vi*sin(radianes)/g)*100)/100.0f;

    ui->lcd4->display(tiempo_max);
    ui->lcd1->display(tiempo_total);
    ui->lcd2->display(desplazamiento);
    ui->lcd3->display(altura_max);


    QSplineSeries *series4 = new QSplineSeries();

    float x,y;

        for(float t = 0.0 ; t<=tiempo_total;t+=0.01){

           x = trunc((vi*cos(radianes)*t)*100)/100.0f;

           y = trunc((vi*sin(radianes)*t-0.5*g*pow(t,2))*100)/100.0f;

           series4->append(x,y);

        }

        QChart *chart3 = new QChart();
        chart3->legend()->hide();
        chart3->addSeries(series4);
        chart3->setTitle("Trayectoria Proyectil (Vector posición.)");
        chart3->createDefaultAxes();
        chart3->axes(Qt::Vertical).first()->setRange(0, altura_max+2);

        QChartView *chartView_3 = new QChartView(chart3);
        chartView_3->setRenderHint(QPainter::Antialiasing);

        ui->graphicsView_3->setChart(chart3);
}

