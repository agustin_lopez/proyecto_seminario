**CALCULADORA DE FUNCIONES Y LANZAMIENTO DE PROYECTILES**

En esta calculadora podras obtener distintos datos relevantes sobre funciones lineales, cuadráticas y además los parámetros involucrados en el lanzamiento de proyectiles.
todo lo anterior con un fin pedadogico, donde el publico que buscamos abarcar, es el alumno de enseñanza media que esta pronto a ingresar a la Universidad.

**Avance HITO 1:**  Para esta entrega se crearon las funciones que seran la estructura de todo el programa. <br>
**Avance HITO 2:**  Para esta entrega añadimos más funciones y además una salida gráfica en QT.
<br>
**Avance HITO 3:**  Para esta entrega mejoramos la salida gráfica implementando una calculadora de áreas, discriminante, vértice y sus respectivos gráficos, tambien
implementamos analisis de lanzamiento de proyectil. 

<img src="https://i.ibb.co/vkKNSTD/diagrama-de-funciones.png"></img>

**Requisitos de compilación:**
	 g++.<br>
¿Como instalar g++?<br>
En la terminal se debe ingresar el siguiente comando: <br>
sudo apt install g++<br>
¿Como compilar nuestra calculadora con g++?<br>
Se debe compilar el archivo main.cpp <br>
En la terminal se debe ingresar una linea de comandos de la forma:
g++ main.cpp -o "NOMBRE DEL COMPILADO" <br>
ejemplo: g++ src/main.cpp -o proyecto <br>
<br>
Para ejecutar el compilado se debe ingresar en la terminal:<br>
./"Nombre del compilado"
<br>
<br>
**Fecha de entrega:**<br>
	HITO 1: 17/10/21.<br>
	HITO 2: 05/12/21.<br>
	HITO 3: 26/12/21.
	
**Librerias utilizadas:**
	 cmath e iostream. 

**Profesor:**
	Nicolas Galvez.

**Asignatura:**
	Seminario de programación.

**Integrantes:**

- 	Agustín López: -> Programador, creacion de README y creacion de CHANGELOG. 
- 	Ignacio Jobse: -> Programador, task tracker.
- 	Nicolas Vergara: -> Programador.


**Rol USM:**
	
-	Nicolas Vergara: 202130538-6
-	Ignacio Jobse: 202130547-5
-	Agustín López: 202130549-1

 	
**Introduccion:	

En este proyecto no solo buscamos poner a prueba los conocimientos adquiridos en el ramo "Seminario de Programacion", sino tambien aplicar lo visto en otras 
materias, en este caso nos enfocamos en dos topicos importantees para el publico al cual esta dirigido el proyecto. El analisis  Matematico y Fisico son dos 
areas que se desean ver en el trabajo, donde se abarca analisis de funciones cuadraticas,lienales y curvas parametricas(proyectil).

**Objetivos:

El objetivo principal del proyecto es dar una herramienta al alumno que este pronto a cursar alguna carrera Tecnico o Universitaria, donde el pueda 
familiarizarse con los topicos que vera en un futuro cercano como estudiante.

**Desarrollo:

Durante el trabajo en el proyecto, nos dimos cuenta que el optimizar recursos, aveces es una manera viable para disminuir la carga sobre lo que conlleva la 
realizacion de una tarea. lo mencionado fue algo que tuvimos que evaluar para el desarrollo del Hito 2, donde como grupo no alcanzamos a cumplir con los objetivos 
estipulados, ya que no teniamos un buen manejo de la herramienta Qt, pero con el pasar de los dias pudimos sobrellevar y con medidas en concreto pudimos finalizar 
el Hito 3 con buenos resultados.

**Conclusion:

Al iniciar el proyecto, teniamos muchas ideas la cuales las podiamos implementar a nuestra aplicacion, pero cuando tuvimos que plasmar todo lo establecido 
en una interfaz grafica, tuvimos que quitar y optimizar las funciones de la misma, es decir, implementar menos funciones, lo cual ayudaba a tener un resultado final
en el timepo establcido. Todo lo elaborado en el Hito 3 nos ayuda directamente a poder tener conocimiento el cual al dia de hoy, es vital en la Industria Tecnologica, 
por lo que es importante manejar estas mismas, en otras palabras Qt es un gran alienado para poder seguir trabanjando de manera indiviual despues de terminar el 
proyecto, todo con el objetivo de indagar aun mas e ir perfeccionando la tecnica.
 
**Referencias:** 

- http://recursostic.educacion.es/descartes/web/materiales_didacticos/funciones_lineales_y_afines_dhafj/funcion_lineal_y_afin1.html
- https://www.problemasyecuaciones.com/funciones/parabolica/funcion-cuadratica-parabolica-vertice-puntos-corte-canonica-factorizada-problemas-resueltos.html
	
